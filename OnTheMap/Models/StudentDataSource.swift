//
//  StudentDataSource.swift
//  OnTheMap
//
//  Created by usertqi on 11/09/2018.
//  Copyright © 2018 roberth. All rights reserved.
//

import UIKit

struct StudentDataSource {
    var studentData = [StudentLocation]()
    static var sharedInstance = StudentDataSource()
}

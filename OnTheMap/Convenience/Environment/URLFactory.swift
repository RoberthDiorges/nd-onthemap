//
//  URLFactory.swift
//  OnTheMap
//
//  Created by usertqi on 11/09/2018.
//  Copyright © 2018 roberth. All rights reserved.
//

import UIKit

class URLFactory: NSObject {
    
    
    class func autentitionUrl() -> String {
        return Environment.sharedInstance().baseUdacityUrl + "/api/session"
    }
    
    class func userInformationUrl(key: String) -> String {
        return Environment.sharedInstance().baseUdacityUrl + "/api/users/\(key)"
    }
    
    class func studentsLocationUrl() -> String {
        return Environment.sharedInstance().baseParseUrl + "/parse/classes/StudentLocation"
    }
}

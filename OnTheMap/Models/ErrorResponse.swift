//
//  ErrorResponse.swift
//  OnTheMap
//
//  Created by usertqi on 11/09/2018.
//  Copyright © 2018 roberth. All rights reserved.
//

import UIKit

struct ErrorResponse {
    
    var error: String?
    var code: String?
    
    init(dictionary: [String : AnyObject]) {
        self.error = dictionary["error"] as? String ?? ""
        self.code = dictionary["code"] as? String ?? ""
    }
    
    init(code: String, error: String) {
        self.code = code
        self.error = error
    }
}

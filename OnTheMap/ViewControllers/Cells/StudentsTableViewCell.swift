//
//  StudentsTableViewCell.swift
//  OnTheMap
//
//  Created by usertqi on 11/09/2018.
//  Copyright © 2018 roberth. All rights reserved.
//

import UIKit

class StudentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mediaUrl: UILabel!
    
    func setup(location: StudentLocation) {
        name.text = location.fullName()
        mediaUrl.text = location.mediaUrl!
    }
}
